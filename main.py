from datetime import datetime


class H:  # H aka History
    history = {}  # timestamp: {by: xx, text: yy}

    @classmethod
    def new_chat(cls, by, t: 't aka text', on_new_chat=None):
        o               = {'by': by, 'text': t}
        H.history[ts()] = o

        if on_new_chat: on_new_chat(o)

    @classmethod
    def print_chat_history(cls):
        print()
        for ts, chat in H.history.items():
            print(f'''{ts} {chat['by']}: {chat['text']}''')


def ts():
    r = datetime.now()
    r = str(r).split('.')[0]
    return r

class by:
    chatbot = 'chatbot'
    user    = '>'


###

def bot_reply(newchat_o):
    '''
    user:    hi
    chatbot: how are you?
    '''
    r = None
    if newchat_o['text'] in ['hi', 'hello']: r = 'how are you?'

    if r:
        H.new_chat(by.chatbot, r)

###
print('''
--- ---
This is a get-started w/ chatbot demo!
Enter quit/exit/:q to exit.
--- ---
''')

H.new_chat(by.chatbot, 'hi')

while True:
    H.print_chat_history()

    print(by.user + ' ', end='')
    t = input()

    H.new_chat(by.user, t, bot_reply)

    if t.lower() in ['exit', 'quit', ':q']:
        break

H.print_chat_history()
H.new_chat(by.chatbot, 'bye')
